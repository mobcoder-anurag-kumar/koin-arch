package com.example.kotlinarch.base

import android.R
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity


abstract class BaseActivity : AppCompatActivity() {

    abstract fun layoutRes(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes())
    }


    fun launchActivity(
        classType: Class<out BaseActivity>
    ) {
        val intent = Intent(this, classType)
        startActivity(intent)
    }

    fun launchActivity(
        classType: Class<out BaseActivity>,
        bundle: Bundle
    ) {
        val intent = Intent(this, classType)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    var progressBar: ProgressBar? = null
    /**
     * Show progress bar with single ocurrent object
     */
    open fun showProgressBar() {
        hideProgressBar()
        if (!this@BaseActivity.isFinishing) {
            val layout =
                findViewById<View>(R.id.content).rootView as ViewGroup

            progressBar = ProgressBar(this, null, R.attr.progressBarStyleLarge)
            progressBar?.isIndeterminate = true
            progressBar?.visibility = View.VISIBLE

            val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
            )

            val rl = RelativeLayout(this)

            rl.gravity = Gravity.CENTER
            rl.addView(progressBar)

            layout.addView(rl, params)
        }
    }

    open fun hideProgressBar() {
        progressBar.let {
            progressBar?.visibility = View.GONE
        }

    }
}