package com.example.kotlinarch.base

import android.app.Application

class App : Application() {

    companion object {
        var INSTANE: Application = App()
    }

    override fun onCreate() {
        super.onCreate()
        INSTANE = this@App
    }
}