package com.example.kotlinarch.ui.splash

import android.os.Bundle
import android.os.Handler
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseActivity
import com.example.kotlinarch.ui.dashboard.base.DashboardActivity
import com.example.kotlinarch.utils.AppConstant

class SplashActivity : BaseActivity() {

    private lateinit var handler: Handler
    private lateinit var runnable: Runnable

    override fun layoutRes(): Int {
        return R.layout.activity_splash
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigateActivity()
    }


    /*
    Wait for 1.5 second to launch a LandingActivity. It is insure to all map icon have already in memory cache
    */
    private fun navigateActivity() {
        handler = Handler()
        runnable = Runnable {
            launchActivity(DashboardActivity::class.java)
            finishAffinity()
        }
        handler.postDelayed(runnable, AppConstant.DEFAULT.SPLASH_TIME)
    }


    override fun onBackPressed() {
        handler.removeCallbacks(runnable)
        super.onBackPressed()
    }

}