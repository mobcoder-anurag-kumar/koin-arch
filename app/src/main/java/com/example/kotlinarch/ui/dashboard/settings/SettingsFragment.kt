package com.example.kotlinarch.ui.dashboard.settings

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseFragment
import com.example.kotlinarch.viewmodel.FlowerViewModel

class SettingsFragment : BaseFragment() {

    private val viewModel: FlowerViewModel by viewModels()

    override fun layoutRes(): Int {
        return R.layout.fragment_settings
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


}