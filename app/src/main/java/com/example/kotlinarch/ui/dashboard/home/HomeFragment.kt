package com.example.kotlinarch.ui.dashboard.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseFragment
import com.example.kotlinarch.ui.home.FlowerAdapter
import com.example.kotlinarch.utils.AppUtil
import com.example.kotlinarch.viewmodel.FlowerViewModel
import kotlinx.android.synthetic.main.activity_flower.*

class HomeFragment : BaseFragment() {

    private lateinit var adapter: FlowerAdapter
    private val viewModel: FlowerViewModel by viewModels()

    override fun layoutRes(): Int {
        return R.layout.fragment_home
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        setObserver()
        viewModel.getFlowersAPI()

    }

    private fun initUI() {
        adapter = FlowerAdapter(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }


    private fun setObserver() {
        viewModel.flowersSuccess.observe(activity, Observer {
            activity.hideProgressBar()
            adapter.setData(it)
        })

        viewModel.errorSuccess.observe(activity, Observer {
            activity.hideProgressBar()
            AppUtil.showToast(it)
        })
    }
}