package com.example.kotlinarch.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseActivity
import com.example.kotlinarch.model.Flower
import com.example.kotlinarch.utils.AppConstant
import com.example.kotlinarch.utils.AppUtil
import com.example.kotlinarch.utils.DialogUtils
import com.example.kotlinarch.utils.GlideUtils
import kotlinx.android.synthetic.main.adapter_flower.view.*

class FlowerAdapter(val activity: BaseActivity) :
    RecyclerView.Adapter<FlowerAdapter.ViewHolder>() {

    private var userProfile: List<Flower>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_flower, parent, false)
        return ViewHolder(v.rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        userProfile?.get(position)?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int {
        if (userProfile == null) {
            return 0
        }
        return userProfile!!.size
    }

    fun setData(users: List<Flower>?) {
        userProfile = users
        notifyDataSetChanged()
    }

    // http://services.hanselandpetal.com/photos
    inner class ViewHolder(private val binding: View) : RecyclerView.ViewHolder(binding) {
        fun bind(flower: Flower) {

            binding.tvName.text = flower.name
            binding.tvDes.text = flower.instructions

            GlideUtils.loadImage(
                binding.ivImage,
                "http://services.hanselandpetal.com/photos/${flower.photo}"
            )

            binding.ivInfo.setOnClickListener(View.OnClickListener { view ->
                run {
                    AppUtil.preventTwoClick(view)
                    DialogUtils.showAlert(activity, flower.instructions)
                }
            })

            binding.ivImage.setOnClickListener {
                val bundle = Bundle()
                bundle.putSerializable(AppConstant.BK.FLOWERS, flower)
                activity.launchActivity(FlowerDetailActivity::class.java, bundle)
            }
        }
    }
}