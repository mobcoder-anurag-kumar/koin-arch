package com.example.kotlinarch.ui.dashboard.base

import android.annotation.SuppressLint
import android.os.Bundle
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseActivity
import com.example.kotlinarch.ui.dashboard.home.HomeFragment
import com.example.kotlinarch.ui.dashboard.more.MoreFragment
import com.example.kotlinarch.ui.dashboard.profile.ProfileFragment
import com.example.kotlinarch.ui.dashboard.settings.SettingsFragment
import com.example.kotlinarch.utils.TabUtils
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity() {

    override fun layoutRes(): Int {
        return R.layout.activity_dashboard;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
    }

    @SuppressLint("Recycle")
    private fun initUI() {
        val adapter = DashBoardAdapter(supportFragmentManager)
        var tabLabel = resources.getStringArray(R.array.dashboard_item)
        var imgSelected = resources.obtainTypedArray(R.array.dashboard_icon_selected);
        var imgUnSelected = resources.obtainTypedArray(R.array.dashboard_icon_unselected);

        val homeFragment = HomeFragment()
        val moreFragment = MoreFragment()
        val profileFragment = ProfileFragment()
        val settingsFragment = SettingsFragment()

        adapter.addFragment(homeFragment)
        adapter.addFragment(moreFragment)
        adapter.addFragment(profileFragment)
        adapter.addFragment(settingsFragment)

        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 4
        tabLayout.setupWithViewPager(viewPager)

        TabUtils.setDashBoardSelection(
            this,
            adapter,
            tabLayout,
            tabLabel,
            imgSelected,
            imgUnSelected
        )

    }

}