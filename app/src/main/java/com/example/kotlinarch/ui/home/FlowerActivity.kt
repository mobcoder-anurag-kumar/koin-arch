package com.example.kotlinarch.ui.home

import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseActivity
import com.example.kotlinarch.utils.AppUtil
import com.example.kotlinarch.viewmodel.FlowerViewModel
import kotlinx.android.synthetic.main.activity_flower.*

class FlowerActivity : BaseActivity() {

    private lateinit var adpater: FlowerAdapter

    private val viewModel: FlowerViewModel by viewModels()


    override fun layoutRes(): Int {
        return R.layout.activity_flower
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUI()
        setObserver()
        showProgressBar()
        viewModel.getFlowersAPI()

    }

    private fun initUI() {
        adpater = FlowerAdapter(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adpater
    }


    private fun setObserver() {
        viewModel.flowersSuccess.observe(this, Observer {
            hideProgressBar()
            adpater.setData(it)
        })

        viewModel.errorSuccess.observe(this, Observer {
            hideProgressBar()
            AppUtil.showToast(it)
        })
    }
}
