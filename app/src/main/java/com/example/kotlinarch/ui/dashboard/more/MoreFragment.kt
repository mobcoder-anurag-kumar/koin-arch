package com.example.kotlinarch.ui.dashboard.more

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseFragment
import com.example.kotlinarch.viewmodel.FlowerViewModel

class MoreFragment : BaseFragment() {

    private val viewModel: FlowerViewModel by viewModels()

    override fun layoutRes(): Int {
        return R.layout.fragment_more
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


}