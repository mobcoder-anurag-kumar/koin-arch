package com.example.kotlinarch.ui.home

import android.os.Bundle
import androidx.activity.viewModels
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseActivity
import com.example.kotlinarch.model.Flower
import com.example.kotlinarch.utils.AppConstant
import com.example.kotlinarch.utils.GlideUtils
import com.example.kotlinarch.utils.PreferenceKeeper
import com.example.kotlinarch.viewmodel.FlowerViewModel
import kotlinx.android.synthetic.main.activity_flower_detail.*

class FlowerDetailActivity : BaseActivity() {


    private val viewModel: FlowerViewModel by viewModels()

    override fun layoutRes(): Int {
        return R.layout.activity_flower_detail
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getIntentData()

        PreferenceKeeper.getInstance().setLat("eger")
    }


    private fun getIntentData() {

        val ff = PreferenceKeeper.getInstance().getLat()

        val bundle = intent.extras
        bundle.let {

            val flower = bundle?.getSerializable(AppConstant.BK.FLOWERS) as Flower

            tvName.text = flower.name

            GlideUtils.loadImage(
                ivImage,
                "http://services.hanselandpetal.com/photos/${flower.photo}"
            )
        }
    }
}
