package com.example.kotlinarch.ui.dashboard.base

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.kotlinarch.base.BaseFragment
import java.util.*

class DashBoardAdapter(manager: FragmentManager?) : FragmentStatePagerAdapter(manager!!) {
    /**
     * Contain all home tabs fragment
     */


    private val baseFragments: MutableList<BaseFragment> =
        ArrayList<BaseFragment>()

    override fun getItem(position: Int): Fragment {
        return baseFragments[position]
    }

    fun addFragment(baseFragment: BaseFragment) {
        baseFragments.add(baseFragment)
    }

    override fun getCount(): Int {
        return baseFragments.size
    }

    fun getCurrentFragment(pos: Int): BaseFragment {
        return baseFragments[pos]
    }
}