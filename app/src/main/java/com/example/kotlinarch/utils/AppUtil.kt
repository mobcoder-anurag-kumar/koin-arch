package com.example.kotlinarch.utils

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.kotlinarch.R
import com.example.kotlinarch.base.App
import com.example.kotlinarch.base.BaseActivity
import java.util.*

object AppUtil {

    fun isConnection(notShowMsg: Boolean): Boolean {
        val connectivityManager = App.INSTANE
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo =
            Objects.requireNonNull(connectivityManager)
                .activeNetworkInfo
        val isNetwork =
            activeNetworkInfo != null && activeNetworkInfo.isConnected
        if (!isNetwork && !notShowMsg) {
            AppUtil.showToast(
                App.INSTANE.resources.getString(
                    R.string.msg_network_connection
                )
            )
        }
        return isNetwork
    }

    fun getColor(color: Int): Int {
        return ContextCompat.getColor(App.INSTANE, color)
    }

    fun showToast(msg: String) {
        msg.let {
            Toast.makeText(App.INSTANE, msg, Toast.LENGTH_LONG).show()
        }
    }

    fun getDeviceId(): String? {
        return Settings.Secure.getString(
            App.INSTANE.getContentResolver(),
            Settings.Secure.ANDROID_ID
        )
    }


    fun setBgView(layout: View, drawableId: Int) {
        val sdk = Build.VERSION.SDK_INT
        if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
            layout.setBackgroundDrawable(ContextCompat.getDrawable(App.INSTANE, drawableId))
        } else {
            layout.background = ContextCompat.getDrawable(App.INSTANE, drawableId)
        }
    }

    fun preventTwoClick(view: View?) {
        if (view != null) {
            view.isEnabled = false
            view.postDelayed(Runnable { view.isEnabled = true }, 800)
        }
    }

    fun shareMessage(activity: BaseActivity, message: String?) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(
            Intent.EXTRA_TEXT,
            message
        )
        sendIntent.type = "text/plain"
        try {
            activity.startActivity(sendIntent)
        } catch (e: Exception) {
        }
    }
}