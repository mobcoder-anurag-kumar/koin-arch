package com.example.kotlinarch.utils

class AppConstant {

    object DEFAULT {
        const val TIME_FORMAT: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        const val SPLASH_TIME: Long =  1000
    }

    object BK {
        const val FLOWERS: String = "FLOWER"
    }

    object DATE_FORMAT {
        const val SUPPORT_LIST = "hh:mm a"
        const val SUPPORT_LIST_EXPORT = "EEE, dd MMM, yyyy, hh:mm a"
    }

    object PKN{
        const val LAT: String = "LAT"
        const val ACCESS_TOKEN: String = "ACCESS_TOKEN"
    }
}