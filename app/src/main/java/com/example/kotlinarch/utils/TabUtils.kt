package com.example.kotlinarch.utils

import android.content.res.TypedArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.kotlinarch.R
import com.example.kotlinarch.base.BaseActivity
import com.example.kotlinarch.base.BaseFragment
import com.example.kotlinarch.ui.dashboard.base.DashBoardAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener

object TabUtils {
    fun setDashBoardSelection(
        activity: BaseActivity,
        adapter: DashBoardAdapter,
        tabLayout: TabLayout,
        tabLabel: Array<String>,
        tabSelectedIcon: TypedArray,
        tabUnSelectedIcon: TypedArray
    ) {


        val bgView =
            arrayOfNulls<View>(tabLayout.tabCount)

        val tvView = arrayOfNulls<TextView>(tabLayout.tabCount)

        val ivView =
            arrayOfNulls<ImageView>(tabLayout.tabCount)

        val slidingTabStrip = tabLayout.getChildAt(0) as ViewGroup
        for (i in 0 until tabLayout.tabCount) {
            val tab = slidingTabStrip.getChildAt(i)
            bgView[i] = tab
            val layoutParams =
                tab.layoutParams as LinearLayout.LayoutParams
            layoutParams.weight = 1f
            tab.layoutParams = layoutParams
            val customTab = LayoutInflater.from(activity).inflate(
                R.layout.layout_tab_dashboard,
                null
            ) as LinearLayout
            val tvName =
                customTab.findViewById<View>(R.id.tv_tab_name) as TextView
            val ivIcon =
                customTab.findViewById<View>(R.id.iv_tab_icon) as ImageView
            tvName.text = tabLabel[i]
            tvView[i] = tvName
            ivView[i] = ivIcon

            if (i == 0) {
                ivIcon.setBackgroundResource(tabSelectedIcon.getResourceId(i, -1))
                tvName.setTextColor(AppUtil.getColor(R.color.color_white))
                tab.background = ContextCompat.getDrawable(activity, R.color.color_tab_select)
            } else {
                tvName.setTextColor(AppUtil.getColor(R.color.color_a0a5a))
                ivIcon.setBackgroundResource(tabUnSelectedIcon.getResourceId(i, -1))
                tab.background = ContextCompat.getDrawable(activity, R.color.color_white)
            }
            tabLayout.getTabAt(i)?.customView = customTab
        }


        // handle tab click with index and set callback on back pressed
        // handle tab click with index and set callback on back pressed
        tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                bgView[tab.position]?.background =
                    ContextCompat.getDrawable(activity, R.color.color_tab_select)
                tvView[tab.position]?.setTextColor(AppUtil.getColor(R.color.color_white))
                ivView[tab.position]?.setBackgroundResource(
                    tabSelectedIcon.getResourceId(
                        tab.position,
                        -1
                    )
                )
                val baseFragment: BaseFragment = adapter.getCurrentFragment(tab.position)

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                bgView[tab.position]?.background =
                    ContextCompat.getDrawable(activity, R.color.color_white)
                tvView[tab.position]?.setTextColor(AppUtil.getColor(R.color.color_a0a5a))
                ivView[tab.position]
                    ?.setBackgroundResource(tabUnSelectedIcon.getResourceId(tab.position, -1))
            }

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }
}