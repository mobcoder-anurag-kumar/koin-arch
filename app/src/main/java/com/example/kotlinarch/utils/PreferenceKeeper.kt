package com.example.kotlinarch.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.kotlinarch.R
import com.example.kotlinarch.base.App

class PreferenceKeeper() {

    init {
        prefs = App.INSTANE.getSharedPreferences(
            App.INSTANE.getString(R.string.app_name), Context.MODE_PRIVATE
        )
    }

    companion object {
        private lateinit var prefs: SharedPreferences
        private lateinit var keeper: PreferenceKeeper

        fun getInstance(): PreferenceKeeper {
            keeper = PreferenceKeeper()
            return keeper
        }
    }


    fun setLat(lat: String) {
        prefs.edit().putString(AppConstant.PKN.LAT, lat).apply()
    }

    fun getLat(): String? {
        return prefs.getString(AppConstant.PKN.LAT, "")
    }

    fun setAccessToken(lat: String) {
        prefs.edit().putString(AppConstant.PKN.ACCESS_TOKEN, lat).apply()
    }

    fun getAccessToken(): String {
        return prefs.getString(AppConstant.PKN.ACCESS_TOKEN, "").toString()
    }
}