package com.example.kotlinarch.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.kotlinarch.R
import com.example.kotlinarch.base.App

object GlideUtils {

    fun loadImage(view: ImageView?, url: String?) {
        if (view != null && url != null) {
            Glide.with(App.INSTANE)
                .load(url)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.ic_launcher_background)
                        .error(R.drawable.ic_launcher_background)
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                )
                .into(view)
        }
    }

    fun loadImageCircle(view: ImageView?, url: String?) {
        if (view != null && url != null) Glide.with(App.INSTANE)
            .load(url)
            .apply(
                RequestOptions()
                    .circleCrop()
                    .skipMemoryCache(false)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
            )
            .into(view)
    }
}