package com.example.kotlinarch.utils

import android.text.TextUtils
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    // 2020-05-05T10:54:02.084Z
    fun getLocalDate(inputDate: String?, outputFormat: String?): String? {
        try {
            if (!TextUtils.isEmpty(inputDate)) {
                var dateToReturn = inputDate
                val sdf =
                    SimpleDateFormat(AppConstant.DEFAULT.TIME_FORMAT)
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val sdfOutPutToSend =
                    SimpleDateFormat(outputFormat)
                sdfOutPutToSend.timeZone = TimeZone.getDefault()
                val gmt = sdf.parse(inputDate)
                dateToReturn = sdfOutPutToSend.format(gmt)
                return dateToReturn
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }
}