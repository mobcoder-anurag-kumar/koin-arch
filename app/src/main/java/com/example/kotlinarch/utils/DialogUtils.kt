package com.example.kotlinarch.utils

import android.content.Context
import android.text.Html
import androidx.appcompat.app.AlertDialog
import com.example.kotlinarch.R
import com.example.kotlinarch.callback.DialogCallback

object DialogUtils {

    fun showAlert(context: Context, message: String) {
        showAlert(
            context,
            context.resources.getString(R.string.app_name),
            message,
            context.resources.getString(R.string.ok),
            context.resources.getString(R.string.cancel)
        )
    }

    fun showAlert(
        context: Context,
        message: String,
        callback: DialogCallback?
    ) {
        showAlert(
            context,
            context.resources.getString(R.string.app_name),
            message,
            context.resources.getString(R.string.ok),
            context.resources.getString(R.string.cancel),
            callback
        )
    }

    fun showAlert(
        context: Context?,
        title: String,
        message: String,
        ok: String?,
        no: String?
    ) {
        showAlert(context, title, message, ok, no, null)
    }

    fun showAlert(
        context: Context?,
        title: String,
        message: String,
        ok: String?,
        no: String?,
        callback: DialogCallback?
    ) {
        val dialog =
            AlertDialog.Builder(context!!)
                .setTitle(Html.fromHtml("<font color='#000'>$title</font>"))
                .setMessage(Html.fromHtml("<font color='#808080'>$message</font>"))
                .setPositiveButton(
                    ok
                ) { _, _ ->
                    callback?.onClick(false)
                }
                .setNegativeButton(
                    no
                ) { _, _ ->
                    callback?.onClick(true)
                }.show()
        dialog.setCanceledOnTouchOutside(false)
        val positiveButton =
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
        val negativeButton =
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
        positiveButton.setTextColor(AppUtil.getColor(R.color.color_black))
        negativeButton.setTextColor(AppUtil.getColor(R.color.color_black))
    }


}