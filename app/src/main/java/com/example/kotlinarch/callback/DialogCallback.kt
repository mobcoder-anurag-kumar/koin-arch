package com.example.kotlinarch.callback

interface DialogCallback {
    fun onClick(isOk: Boolean)
}