package com.example.kotlinarch.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kotlinarch.api.APICallHandler
import com.example.kotlinarch.api.APICallManager
import com.example.kotlinarch.api.APIClient
import com.example.kotlinarch.api.APIType
import com.example.kotlinarch.model.Flower

class FlowerViewModel : ViewModel(), APICallHandler<Any> {

    val flowersSuccess = MutableLiveData<List<Flower>>()
    val flowerDetailSuccess = MutableLiveData<Flower>()

    val errorSuccess = MutableLiveData<String>()

    fun getFlowersAPI() {
        val api = APICallManager(APIType.FLOWERS, this, APIClient.getRequest())
        api.getFlowersAPI()
    }

    override fun onSuccess(apiType: APIType, response: Any) {

        when (apiType) {
            APIType.FLOWERS -> {
                val flowers = response as List<Flower>
                flowersSuccess.value = flowers
            }

            APIType.FLOWER_DETAIL -> {
                val flower = response as Flower
                flowerDetailSuccess.value = flower
            }
        }
    }

    override fun onFailure(apiType: APIType, code: Int, error: String) {
        errorSuccess.value = error
    }

}