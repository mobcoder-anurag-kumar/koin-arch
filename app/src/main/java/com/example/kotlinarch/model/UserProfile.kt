package com.example.kotlinarch.model

data class UserProfile(var name: String) {
    var age: Int = 0
    var address: String = ""
}