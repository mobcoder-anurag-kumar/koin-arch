package com.example.kotlinarch.model

import java.io.Serializable

data class Flower(
    val category: String,
    val instructions: String,
    val name: String,
    val photo: String,
    val price: Double,
    val productId: Int
) : Serializable