package com.example.kotlinarch.api

import android.os.Handler
import com.example.kotlinarch.model.Flower
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class APICallManager<T>(
    private val apiType: APIType,
    private val apiCallHandler: APICallHandler<T>,
    private val repo: IApiService
) : Callback<T> {

    var handler: Handler = Handler()
    lateinit var runnable: Runnable

    init {
        runnable = Runnable {
            handler.removeCallbacks(runnable)
            apiCallHandler.onFailure(apiType, 0, "fefv3 e3wefwef")
        }

        handler.postDelayed(runnable, 80000)
    }


    override fun onResponse(call: Call<T>, response: Response<T>) {
        handler.removeCallbacks(runnable)
        response.body()?.let { apiCallHandler.onSuccess(apiType, it) }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        handler.removeCallbacks(runnable)
        apiCallHandler.onFailure(apiType, 0, "wefwef")
    }


    fun getFlowersAPI() {
        val call = repo.getFlowersAPI()
        call.enqueue(this@APICallManager as Callback<List<Flower>>)
    }
}
