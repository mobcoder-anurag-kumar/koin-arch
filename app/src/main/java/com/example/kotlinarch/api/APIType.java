package com.example.kotlinarch.api;

public enum APIType {
    FLOWERS,
    FLOWER_DETAIL
}
