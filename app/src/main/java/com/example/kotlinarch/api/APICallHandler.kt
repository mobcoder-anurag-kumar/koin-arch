package com.example.kotlinarch.api

interface APICallHandler<T> {

    fun onSuccess(apiType: APIType, response: T)

    fun onFailure(apiType: APIType, code: Int, error: String)
}