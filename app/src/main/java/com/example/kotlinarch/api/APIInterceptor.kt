package com.example.kotlinarch.api

import android.text.TextUtils
import android.util.Log
import com.example.kotlinarch.utils.PreferenceKeeper
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class APIInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = PreferenceKeeper.getInstance().getAccessToken()
        Log.i("MMMMMMMMM", "ACCESS TOKEN $token")
        val originalRequest = chain.request()
        if (TextUtils.isEmpty(token)) {
            val builder = originalRequest.newBuilder()
            val oldReq = builder
                .addHeader("authorization", "Basic Ym9sc3Rlcl9hZG1pbjphZG1pbkBib2xzdGVy")
                .build()
            return chain.proceed(oldReq)
        }
        val builder = originalRequest.newBuilder()
        val oldReq = builder
            .addHeader("accessToken", token)
            .addHeader("authorization", "Basic Ym9sc3Rlcl9hZG1pbjphZG1pbkBib2xzdGVy")
            .build()
        return chain.proceed(oldReq)
    }
}