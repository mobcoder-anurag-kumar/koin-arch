package com.example.kotlinarch.api

import com.example.kotlinarch.model.Flower
import retrofit2.Call
import retrofit2.http.GET

interface IApiService {

    @GET("http://services.hanselandpetal.com/feeds/flowers.json")
    fun getFlowersAPI(): Call<List<Flower>>
}